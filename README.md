# chickoray

**Chicken**(scheme) over **Raylib**

```                                                
          \'-.      _.-"/ 
     (\    :  :    /   /
      \'-.  i |   /   / 
       '. \  \i  /_.-" 
         '-'-.; /"
              \/ 
    Chick   ('""')
             \O /    Ray(lib)
              )(
             i__i
       fsc  i____i                                                            
```

----
Copyright (c) 2022- Frederic Peschanski (@fredokun) under the Zlib License
(cf. LICENSE file)
